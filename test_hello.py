from unittest import TestCase
from hello import say_hello

class Test(TestCase):

    def test_say_hello(self):
        name = "John"
        result = say_hello(name)
        self.assertEqual(result, "Hello John")
