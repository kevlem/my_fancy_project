def say_hello(name):
    """A function that returns hello
    
    :param name: name of a person
    :type name: str
    :return: a greeting
    :rtype: str
    """
    return "Hello " + name
